import cv2
import mediapipe as mp

mp_drawing = mp.solutions.drawing_utils
mp_objectron = mp.solutions.objectron

cap = cv2.VideoCapture(0)

with mp_objectron.Objectron (
    static_image_mode = True,
    max_num_objects = 1,
    min_detection_confidence = 0.7,
    model_name = "Cup") as objectron:
    

    while True:
        ret, frame = cap.read()
        if ret == False:
            break

        
        results = objectron.process(frame)

        if results.detected_objects is not None:
            for detected_object in results.detected_objects:
                mp_drawing.draw_landmarks(frame, detected_object.landmarks_2d, mp_objectron.BOX_CONNECTIONS,
                mp_drawing.DrawingSpec(color=(0,0,255), thickness = 2, circle_radius = 2),
                mp_drawing.DrawingSpec(color=(0,255,0), thickness = 2))

        cv2.imshow("Cam", frame)
        if cv2.waitKey(1) & 0xFF == 27:
            break 

cap.release()
cv2.destroyAllWindows()
