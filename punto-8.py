import cv2
import mediapipe as mp
import numpy as np


mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands
cap = cv2.VideoCapture(0)

finger_point = 8
# Puntos
# [8, 8, 12, 16, 20]


palm_points = [0, 1, 2, 5, 9, 13, 17]
with mp_hands.Hands(
    model_complexity=1,
    max_num_hands=2,
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5) as hands:

    while True:
        ret, frame = cap.read()
        if ret == False:
            break
        frame = cv2.flip(frame, 1)
        height, width, _ = frame.shape
        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        results = hands.process(frame_rgb)

        if results.multi_hand_landmarks:
            coordinates_ft = []
            coordinates_palm = []
            i = 1
            for hand_landmarks in results.multi_hand_landmarks:

                
                x = int(hand_landmarks.landmark[finger_point].x * width)
                y = int(hand_landmarks.landmark[finger_point].y * height)

                cv2.circle(frame, (x, y), 3, (0, 255, 0), 5)
                cv2.rectangle(frame, (x - 10, y - 10), (x - 30, y - 30), (0, 0, 255), -1)
                cv2.putText(frame, ("Punto Num: " + str(finger_point)),(x, y - 10), 1, 2, (0, 0, 255), 2)

                
                cv2.putText(frame, ("Coordenadas: x=" + str(x) + ", y=" + str(y)),(0, 10 * i), 1, 1, (0, 0, 255), 2)
                i+=1
                if (i == 3) :
                    i = 1
            
        cv2.imshow("Cam", frame)
        if cv2.waitKey(1) & 0xFF == 27:
            break

cap.release()
cv2.destroyAllWindows()


            



